"use strict";

const { describe, beforeEach, afterEach, it } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
require( "should" );

describe( "Limiting rate of requests per seconds WITHOUT deferral queue", () => {
	const ctx = {};

	afterEach( SDT.after( ctx ) );
	beforeEach( SDT.before( ctx, {
		plugin: true,
		files: {
			"config/all.js": `
module.exports = function() { 
	return { 
		policies: { 
			"/foo": this.runtime.service.RateLimiter.limitPerSecond( 3 ),
		},
		routes: {
			"/foo/bar": ( _, res ) => res.send( "foo!" ),
			"/boo": ( _, res ) => res.send( "boo!" ),
		},
	};
};
`,
		},
		options: {
			// debug: true,
		},
	} ) );

	it( "is ignored on requests addressing separate scope", () => {
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/boo" ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "boo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is dropping additional requests received in a second", () => {
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/foo/bar" ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 3 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 3 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 7 );
			} );
	} );

	it( "is passing additional requests received every other second", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 1200 * Math.floor( index / 3 ) ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 3200 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is limiting number of requests in a rolling time frame", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 180 * index ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 1500 );

				responses.length.should.be.equal( 10 );
				// first three pass ... then two fail until first three get old ... another three pass with final two failing again
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 6 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 6 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 4 );
			} );
	} );
} );

describe( "Limiting rate of requests per seconds WITH deferral queue of SUFFICIENT size", () => {
	const ctx = {};

	afterEach( SDT.after( ctx ) );
	beforeEach( SDT.before( ctx, {
		plugin: true,
		files: {
			"config/all.js": `
module.exports = function() { 
	return { 
		policies: { 
			"/foo": this.runtime.service.RateLimiter.limitPerSecond( 3, { queueSize: 20 } ),
		},
		routes: {
			"/foo/bar": ( _, res ) => res.send( "foo!" ),
			"/boo": ( _, res ) => res.send( "boo!" ),
		},
	};
};
`,
		},
		options: {
			// debug: true,
		},
	} ) );

	it( "is ignored on requests addressing separate scope", () => {
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/boo" ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "boo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is not dropping additional requests received in a second but deferring them accordingly", function() {
		this.timeout( 4000 );

		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/foo/bar" ) ) )
			.then( responses => {
				( Date.now() - start ).should.not.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is passing additional requests received every other second", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 1200 * Math.floor( index / 3 ) ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 3200 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is deferring requests additionally received in a rolling time frame, thus delivering all requests eventually", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 180 * index ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 1500 );

				responses.length.should.be.equal( 10 );
				// all pass due to deferring extra requests per second rather than rejecting any
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );
} );

describe( "Limiting rate of requests per seconds WITH deferral queue of INSUFFICIENT size", () => {
	const ctx = {};

	afterEach( SDT.after( ctx ) );
	beforeEach( SDT.before( ctx, {
		plugin: true,
		files: {
			"config/all.js": `
module.exports = function() { 
	return { 
		policies: { 
			"/foo": this.runtime.service.RateLimiter.limitPerSecond( 3, { queueSize: 2 } ),
		},
		routes: {
			"/foo/bar": ( _, res ) => res.send( "foo!" ),
			"/boo": ( _, res ) => res.send( "boo!" ),
		},
	};
};
`,
		},
		options: {
			// debug: true,
		},
	} ) );

	it( "is ignored on requests addressing separate scope", () => {
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/boo" ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "boo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is deferring additional requests received in a second, but dropping them on deferral queue overflow", function() {
		this.timeout( 4000 );

		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( () => ctx.get( "/foo/bar" ) ) )
			.then( responses => {
				( Date.now() - start ).should.not.be.lessThan( 1000 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 5 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 5 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 5 );
			} );
	} );

	it( "is passing additional requests received every other second", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 1200 * Math.floor( index / 3 ) ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 3200 );

				responses.length.should.be.equal( 10 );
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 10 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 0 );
			} );
	} );

	it( "is limiting number of requests in a rolling time frame", function() {
		this.timeout( 6000 );

		const delay = time => new Promise( resolve => setTimeout( resolve, time ) );
		const start = Date.now();

		return Promise.all( new Array( 10 ).fill( 0 ).map( ( _, index ) => delay( 180 * index ).then( () => ctx.get( "/foo/bar" ) ) ) )
			.then( responses => {
				( Date.now() - start ).should.be.greaterThan( 1500 );

				responses.length.should.be.equal( 10 );
				// first three pass ... then two are deferred ... another one comes early before first passed one gets old -> rejected
				// ... after that the same procedure is repeated with a delay
				responses.filter( response => response.text === "foo!" ).length.should.be.equal( 8 );
				responses.filter( response => response.statusCode === 200 ).length.should.be.equal( 8 );
				responses.filter( response => response.statusCode === 503 ).length.should.be.equal( 2 );
			} );
	} );
} );
