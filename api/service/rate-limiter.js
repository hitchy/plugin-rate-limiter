"use strict";

module.exports = function() {
	/** @type HitchyAPI */
	const api = this;

	const logDebug = api.log( "hitchy:rate-limiter:debug" );
	const logInfo = api.log( "hitchy:rate-limiter:info" );

	const isNonNegativeInteger = /^\s*\+?\d*\s*$/;
	const isPositiveInteger = /^\s*\+?[1-9]\d*\s*$/;

	/**
	 * Implements functions for generating policy routing handlers each limiting
	 * request rates in a particular way.
	 */
	class RateLimiter {
		/**
		 * Generates policy routing handler forwarding limited number of
		 * requests per timeframe, deferring some requests hitting this limit
		 * and rejecting any additional request with status 503.
		 *
		 * @param {number} requestsPerTime number of requests to accept per timeframe
		 * @param {number} timeframe duration of timeframe in seconds
		 * @param {number} queueSize number of requests to defer at most on hitting rate limit
		 * @param {number} retryAfter number of seconds to suggest for waiting before trying again
		 * @returns {function(req:HitchyIncomingMessage, res:HitchyServerResponse, next:function): void} generated handler function
		 */
		static limitPerTime( requestsPerTime, { timeframe = 1, queueSize = 0, retryAfter = 10 } = {} ) {
			if ( !isPositiveInteger.test( requestsPerTime ) ) {
				throw new TypeError( "number of accepted requests per timeframe must be positive integer" );
			}

			if ( !isPositiveInteger.test( timeframe ) ) {
				throw new TypeError( "size of timeframe in seconds must be positive integer" );
			}

			if ( !isNonNegativeInteger.test( queueSize ) ) {
				throw new TypeError( "size of queue containing request hold-back must be non-negative integer" );
			}

			if ( !isPositiveInteger.test( retryAfter ) ) {
				throw new TypeError( "suggested number of seconds to wait before retrying must be positive integer" );
			}

			const requestTimestamps = [];
			const deferralQueue = [];
			const timeframeMS = timeframe * 1000;
			let drainHandle;

			const cleanTimestamps = now => {
				let i;

				// remove timestamps of requests that are older than configured
				// timeframe assuming their are collected from oldest to newest
				// - find first timestamp that has been tracked in configured sliding timeframe
				for ( i = 0; i < requestTimestamps.length; i++ ) {
					if ( requestTimestamps[i] + timeframeMS >= now ) {
						break;
					}
				}

				if ( i < 0 ) {
					// there is no timestamp in sliding timeframe -> remove them all
					requestTimestamps.splice( 0 );
				} else if ( i > 0 ) {
					// found first timestamp in timeframe -> remove all timestamps preceding it (being older)
					requestTimestamps.splice( 0, i < 0 ? requestTimestamps.length : i );
				}
			};

			const drainDeferred = now => {
				while ( deferralQueue.length > 0 && requestTimestamps.length < requestsPerTime ) {
					requestTimestamps.push( now );
					deferralQueue.shift()();
				}
			};

			return ( req, res, next ) => {
				const now = Date.now();

				cleanTimestamps( now );

				logDebug( "got %d requests within last %d second(s)", requestTimestamps.length, timeframe );


				// drain queue of deferred requests as much as possible first
				if ( deferralQueue.length > 0 ) {
					clearTimeout( drainHandle );
					drainDeferred( now );
				}


				if ( requestTimestamps.length >= requestsPerTime ) {
					// got too many requests within current sliding timeframe
					if ( deferralQueue.length < queueSize ) {
						// --> defer current request
						logInfo( "deferring request after getting more than %d requests within %d second(s)", requestsPerTime, timeframe );
						deferralQueue.push( next );
					} else {
						// --> drop current request as deferral queue is full
						logInfo( "rejecting request after getting more than %d requests within %d second(s)", requestsPerTime, timeframe );
						res.status( 503 ).set( "Retry-After", retryAfter ).json( {
							error: "service unavailable",
						} );
					}
				} else {
					// rate limit in current sliding timeframe hasn't been met
					// -> track request's time and let it pass
					requestTimestamps.push( now );
					next();
				}


				// drain deferred requests even when there are no more requests
				if ( deferralQueue.length > 0 ) {
					const drainFn = () => {
						logInfo( "draining queue of deferred requests, found %d request(s)", deferralQueue.length );

						const then = Date.now();

						cleanTimestamps( then );
						drainDeferred( then );

						if ( deferralQueue.length > 0 ) {
							drainHandle = setTimeout( drainFn, requestTimestamps[0] + timeframeMS - then );
						}

						logInfo( "drained queue of deferred requests, kept %d request(s)", deferralQueue.length );
					};

					clearTimeout( drainHandle );

					drainHandle = setTimeout( drainFn, requestTimestamps[0] + timeframeMS - now );
				}
			};
		}

		/**
		 * Generates policy routing handler forwarding limited number of
		 * requests per second, deferring some requests hitting this limit and
		 * rejecting any additional request with status 503.
		 *
		 * @param {number} requestsPerSecond number of requests to accept per second
		 * @param {number} queueSize number of requests to defer at most on hitting rate limit
		 * @param {number} retryAfter number of seconds to suggest for waiting before trying again
		 * @returns {function(req:HitchyIncomingMessage, res:HitchyServerResponse, next:function): void} generated handler function
		 */
		static limitPerSecond( requestsPerSecond, { queueSize = 0, retryAfter = 10 } = {} ) {
			return this.limitPerTime( requestsPerSecond, { timeframe: 1, queueSize, retryAfter } );
		}

		/**
		 * Generates policy routing handler forwarding limited number of
		 * requests per minute rejecting any additional request with status 503.
		 *
		 * @param {number} requestsPerMinute number of requests to accept per minute
		 * @param {number} retryAfter number of seconds to suggest for waiting before trying again
		 * @returns {function(req:HitchyIncomingMessage, res:HitchyServerResponse, next:function): void} generated handler function
		 */
		static limitPerMinute( requestsPerMinute, { retryAfter = 120 } = {} ) {
			return this.limitPerTime( requestsPerMinute, { timeframe: 60, queueSize: 0, retryAfter } );
		}

		/**
		 * Generates policy routing handler forwarding limited number of
		 * requests per hour rejecting any additional request with status 503.
		 *
		 * @param {number} requestsPerHour number of requests to accept per hour
		 * @param {number} retryAfter number of seconds to suggest for waiting before trying again
		 * @returns {function(req:HitchyIncomingMessage, res:HitchyServerResponse, next:function): void} generated handler function
		 */
		static limitPerHour( requestsPerHour, { retryAfter = 900 } = {} ) {
			return this.limitPerTime( requestsPerHour, { timeframe: 3600, queueSize: 0, retryAfter } );
		}
	}

	return RateLimiter;
};
